package com.regidor.demographics.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.regidor.demographics.entities.PersonEntity;
import com.regidor.demographics.objects.Person;
import com.regidor.demographics.repository.PersonRepository;

@Controller
public class IndexController {
	
	@Autowired
	private PersonRepository personRepository;
	
	@RequestMapping("/")
	public String index() {
		return "index";
	}
	
	@RequestMapping(value="/form", method=RequestMethod.GET)
	public String index(Person person) {
		return "form";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNewPost(@Valid Person person, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "index";
		}
		
		PersonEntity personExist = personRepository.findOne(person.getPPS());
		if(personExist==null) {
			personRepository.save(new PersonEntity(person.getName(),person.getPPS(),person.getBirth(),person.getPhone()));
		}
		else{
			model.addAttribute("text", "There is a person with the same PPS");
			return "index";
		}
		return "redirect:list";
		
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String showAllPosts(Model model) {
		model.addAttribute("persons", personRepository.findAll());
		return "list";
	}
}