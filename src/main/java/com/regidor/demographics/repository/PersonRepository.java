package com.regidor.demographics.repository;

import org.springframework.data.repository.CrudRepository;

import com.regidor.demographics.entities.PersonEntity;

public interface PersonRepository  extends CrudRepository <PersonEntity, String> {
}