package com.regidor.demographics.entities;


import javax.persistence.Entity;
import javax.persistence.Id;





 
@Entity
public class PersonEntity {
	
	public PersonEntity() {}
	
	public PersonEntity(String name, String PPS,  String birth, String phone ) {
		this.name = name;
		this.PPS = PPS;
		this.birth = birth;
		this.phone = phone;
	}
	
	@Id
	private String PPS;
 
	private String name;
	
	private String birth;
	
	private String phone;

	public String getPPS() {
		return PPS;
	}

	public void setPPS(String pPS) {
		PPS = pPS;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}